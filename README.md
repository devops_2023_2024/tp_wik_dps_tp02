# Projet Docker avec API Express

Ce projet vise à dockeriser une API développée avec Express en utilisant Docker. Deux configurations d'images Docker sont fournies, l'une avec un seul stage et l'autre avec plusieurs stages.

## Docker avec un seul stage

### Construction de l'image
```bash
docker build -t nom_image_api:latest .
```

### Analyse des vulnérabilités

docker scan étant obsolète je propose d'utiliser docker scout.

```bash
docker scout quickview nom_image_api:latest
```

### Exécution de l'image

```bash
docker run -p 3000:3000 nom_image_api:latest
```

## Docker avec plusieurs stages

### Construction de l'image

```bash
docker build -t nom_image_api_multistage:latest -f Dockerfile.multistage .
```

### Analyse des vulnérabilités

```bash
docker scout quickview nom_image_api_multistage:latest
```

### Exécution de l'image

```bash
docker run -p 3000:3000 nom_image_api_multistage:latest
```
![](./img/its-up-to-you-frank-sinatra.gif)