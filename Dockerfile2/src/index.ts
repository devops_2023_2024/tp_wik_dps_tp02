// On importe le module Express
const express = require('express');

// On crée une instance d'Express pour définir les routes et les gestionnaires d'événements
const app = express();

// On définit une route pour les requêtes HTTP GET sur le chemin /ping
app.get('/ping', (req: { headers: any; }, res: { json: (arg0: { headers: any; }) => void; }) => {
  // On récupére les headers de la requête
  const headers = req.headers;

  // On retourne les headers au format JSON
  res.json({ headers });
});

app.use((_req: any, res: { status: (arg0: number) => { (): any; new(): any; send: { (arg0: string): void; new(): any; }; }; }) => {
  res.status(404).send("");
});

// On dit qu'on écoute sur le port 3000
app.listen(3000, () => {
  console.log("Server is running on port 3000");
});
